## Name
eunis2pft

## Description
This repository contains functions to calculate PFT (plant functional type) fractions from EUNIS classes. The PFT fraction per EUNIS class is contained in a lookup table in /data. The spatial distribution of EUNIS classes is given in /data as well as a shapefile for the respective level. The function modify surfdata takes a CLM surfdata file and replaces the PFT fractions with those derived from EUNIS.

## Usage
### Example script to calculate PFT fractions from a given EUNIS class

load the functions
```R
source("./functions/PftfractionsFromEUNIS.R")
source("./functions/PftFromCover.R")
source("./functions/adjustCover.R")
```

set the EUNIS level
```R
level <- 3
```


load the data containing the measured PFT cover per Eunis class per plot
```R
pfts <- c("BDS.Boreal", "BDS.Temperate", "BDT.Boreal", "BDT.Temperate",
            "BES.Boreal", "BES.Temperate", "BES.Tropical", "BET.Temperate",
            "C3.arctic", "C3.Grass", "C4.Grass", "NDT.Boreal",
            "NES.Boreal", "NES.Temperate", "NET.Boreal")

covertableplots <- read.csv("./data/esyPFTs_v1.3.csv", sep=";")
covertableplots <- covertableplots[complete.cases(covertableplots),]
covertableplots$Bare_ground <- as.numeric(covertableplots$Bare_ground)
covertableplots$water <- as.numeric(covertableplots$water)
```

aggregate the data
```R
lookuptable <- PftFromCover(covertableplots)
```


set the Eunis class and calculate the cover
```R
eunisclass = "R1A"

PftfractionFromEUNIS(eunisclass, lookuptable, level)
```






### Example script to calculate PFTs from EUNIS classes and modifies the CLM surfdata file

load the functions and libraries
```R
source("./functions/GetPftFraction.R")
source("./functions/PftfractionsFromEUNIS.R")
source("./functions/SpEunis2Pft.R")
source("./functions/modifySurfdata.R")
source("./functions/eunistif2shp.R")
source("./functions/PftFromCover.R")
source("./functions/adjustCover.R")
source("./functions/aggfun.R")
library(raster)
library(ncdf4)
```


set the EUNIS level
```R
level <- 3
```


load the data containing the measured PFT cover per Eunis class per plot
```R
pfts <- c("BDS.Boreal", "BDS.Temperate", "BDT.Boreal", "BDT.Temperate",
            "BES.Boreal", "BES.Temperate", "BES.Tropical", "BET.Temperate",
            "C3.arctic", "C3.Grass", "C4.Grass", "NDT.Boreal",
            "NES.Boreal", "NES.Temperate", "NET.Boreal")

covertableplots <- read.csv("./data/esyPFTs_v1.3.csv", sep=";")
covertableplots <- covertableplots[complete.cases(covertableplots),]
covertableplots$Bare_ground <- as.numeric(covertableplots$Bare_ground)
covertableplots$water <- as.numeric(covertableplots$water)
```

aggregate the data
```R
lookuptable <- PftFromCover(covertableplots)
```


load the spatially distributed eunis data and create a spatial points dataset from it
```R
legend <- read.table("./data/eunis_lev3_legend.txt", header=T)
input <- "./data/eunis_map_lev3_full.tif"
eunis <- terra::vect(eunistif2shp(input, legend))
```


create a table with the CLM PFT names
```R
eunisdf <- data.frame(matrix(ncol=2,nrow=19))
colnames(eunisdf) <- c("ITV", "PFT")
eunisdf$PFT <- c("Bare Ground", "NET Temperate", "NET Boreal", "NDT Boreal", "BET Tropical", 
                 "BET Temperate", "BDT Tropical", "BDT Temperate", "BDT Boreal", "BES Temperate",
                 "BDS Temperate", "BDS Boreal", "C3 arctic", "C3 Grass", "C4 Grass", "UCrop UIrr", 
                 "UCrop Irr", "Crop UIrr", "Crop Irr")
eunisdf$ITV <- seq(0,18,1)
```

Harmonize data names and get CRS
```R
eunis_2 <- terra::vect("./data/lvl2.shp")
crs_in <- terra::crs(eunis_2)
terra::crs(eunis) <- terra::crs(eunis_2)
names(eunis) <- "Habitat"
eunis$Habitat <- toupper(eunis$Habitat)
lookuptable$EUNIS_l3 <- toupper(lookuptable$EUNIS_l3)
```

set names of surface data file and land sea mask
```R
surfdata <- "/home/karger/surfdata_0.1x0.1_hist_16pfts_Irrig_CMIP6_simyr2000_c200915_eunis_l3.nc"
landseamask <- "./data/landseamask.tif"
```

loop over the PFTS and modify the CLM surfdata file
```R
for (i in c(0:15)) {
    print(i)
    pft <- eunisdf$PFT[i+1]
    fileout <- paste0("~/",gsub(" ","_",pft),"_l3_df.txt")
    if (file.exists(fileout) == FALSE) {
        df <- SpEunis2Pft(eunis, lookuptable, level, pft, use.ivt=FALSE, parallel=TRUE)
        write.table(df, file=fileout, sep="\t", row.names=F)
    } else {
        df <- read.table(fileout, header=T, sep="\t")
    }
    df[,1] <- df[,1]*100
    modifySurfdata(df, surfdata, i, crs_in, landseamask)
}
```

## Replace CLM PFT fractions with NFF-EUNIS based PFT fractions 

This script illustrates the workflow to modify the PFT and PCT fractions for CLM 
surface data. Several steps are neccessary for this, and they are embedded in 
seperate R scripts. 

### Calculate PFT fractions based on vegetation surveys

The routine for to calculate PFT fractions from EUNIS class specific vegetation
surveys is found in the "nff2pft.R" file.

Load the libraries needed:
```R
library(raster)
library(viridis)
library(parallel)
source("./functions/PftfractionFromEUNIS3nff.R")
source("./functions/calcPFTrasterEUNIS3nff.R")
```

Read in the EUNIS vegetation survey data and do some data crunching:

```R
df <- as.data.frame(read.csv("./data/esyPFTs_v1.3.csv", sep=";"))

df$Bare_ground <- as.numeric(df$Bare_ground)

df$water <- as.numeric(df$water)

df$BES.Tropical <- as.numeric(df$BES.Tropical)

```

Create a crosswalk table by aggregating the PFTs per EUNIS level 3 class 
using the mean:

```R
cwt <- aggregate(cbind(Bare_ground, water, BDS.Boreal, BDS.Temperate, 
                       BDT.Boreal, BDT.Temperate, BES.Boreal, BES.Temperate, 
                       BES.Tropical, BET.Temperate, C3.arctic, C3.Grass, 
                       C4.Grass, NDT.Boreal, NES.Boreal, NES.Temperate, 
                       NET.Boreal , NET.Temperate) ~ EUNIS_l3, data = df, 
                 FUN = mean, na.rm = T)
```

Read in the legend of the eunis data that contains the "mapid" of the spatial
NFF EUNIS maps and combine it with the crosswalk table "cwt". Remove incomplete
rows:
```R
eunis_legend <- read.csv("../data/eunis_legend.csv", header=T)

lookuptable <- merge(cwt, eunis_legend[,2:3], 
                     by.x="EUNIS_l3", by.y="EUNIS_2020_code", all=T)

lookuptable <- lookuptable[complete.cases(lookuptable), ]
```

Test if everything works by readint in one eunis nff map, cropping it to 
a smaller extent, and then calculate the PFT fractions for it for a 
specific IVT code:
```R
eunis <- raster("../data/eunis_map_nac.tif")
ext <- c(45e+05,46e+05,25e+05,26e+05)
eunistest<- raster::crop(eunis, ext)


eunistest <- terra::rast(eunistest)
eunistest <- c(eunistest, eunistest)
out <- terra::app(eunistest, 
                  fun=function(x){
                    calcPFTrasterEUNIS3nff(x,
                                           IVT=0,
                                           lookuptable = lookuptable)})
```

plot the results:
```R
plot(out, col=viridis(100))
```


Now run the entire script over all NFF EUNIS maps and calculate the PFT
fractions. The PFT fractions will be saved in a seperate GEOTiff file. Here
we use the parallel package to speed things up. We create a cluster with
38 cores and then loop over all NFF scenarios. 
```R
# create a cluster
cl <- makeCluster(38) 

# loop over all scenarios
for (nff in c("nac","nfn","nfs","ssp1")){
  eunis <- raster(paste0("./data/eunis_map_",nff,".tif"))
  st2 <- terra::rast(eunis)
  st2 <- c(st2, st2)
  for (ivt in seq(0,14,1)) {
    clusterExport(cl, c("PftfractionFromEUNIS3nff", "calcPFTrasterEUNIS3nff" ,
                  "lookuptable", "ivt", "adjustCover"))
    assign(paste0("PFT",ivt), 
           terra::app(st2, 
                      fun=function(x){
                        calcPFTrasterEUNIS3nff(x, 
                                               IVT=ivt, 
                                               lookuptable = lookuptable)},
                      cores=cl))
  }
  pftstack <- c(PFT0, PFT1, PFT2, PFT3, PFT4, PFT5, PFT6, PFT7, PFT8,
                PFT9, PFT10, PFT11, PFT12, PFT13, PFT14)
  writeRaster(pftstack, 
              file=paste0("/home/karger/scratch/",nff,"_pftstack.tif"), 
              overwrite=T)
}
```

### Correct the missing EUNIS classes 

Some EUNIS classes do not have vegetation survey survey data attached to it
so we need to correct for that. First read in the file that has contains the 
PFT fractions for the missing EUNIS classes and then loop over all NFF 
pft fractions to correct them. This will create a set of corrected GEOTiffs.
The script is found in the file "correctNFFPFTs.R" :
```R
eunis_missing_fill <- read.csv("./data/missing_eunis_fill.txt", 
                               header=T, sep="\t")

# loop over all NFFs
for (nff in c("ssp1","nfn","nac","nfs")){
  nffst <- stack(paste0("/mnt/lud11/karger/scratch/",nff,"_pftstack.tif"))
  eunis <- raster(paste0("./data/eunis_map_",nff,".tif"))
  nst <- stack()
  for (itv in 0:14){
    print(itv)
    ras1 <- nffst[[itv+1]]
    for (mapid in unique(eunis_missing_fill$mapid)){
      pftfrac <- eunis_missing_fill[eunis_missing_fill$mapid == mapid, 
                                    paste0("X", itv)]
      ras1[eunis==mapid] <- pftfrac/100
      }
      nst <- stack(nst, ras1)
  }
nst <- terra::rast(nst)
terra::writeRaster(nst, 
                   paste0("/home/karger/scratch/",
                          nff,"_pftstack_cormisseunis.tif"), overwrite=T)
}
```

### Replace the PFT fractions in the surfdata files

With the PFT fractions now correct and summing up to 100%, we can modify
the surfdata NetCDF files. First, read the required libraries and functions:
```R
library(ncdf4)
source("./functions/modifySurfdata.R")
```

Then we need to load the data and some auxilary files (landseamask, and a 
vector data file containing the CRS of the eunis maps):
```R
# read in auxilarry data
landseamask <- "./data/landseamask.tif"
eunis_2 <- terra::vect("./data/lvl2.shp")
crs_in <- terra::crs(eunis_2)
eunismask <- raster('./data/eunis_map_ssp1.tif')
```

Now we loop over all NFF scenarios. This loop copies the original surfdata file
to a new location, creating a separate file for each NFF, and then modifies
the PFT fractions in it:
```R
# loop over all NFF scenarios
for (nff in c("nac","nfn","nfs","ssp1")){
    surfdata_org <- paste0("/storage/
                           karger/feedbacks/
                           surfdata_0.1x0.1_EUR_hist_16pfts_Irrig_
                           CMIP6_simyr2005_c230523.nc")
    
    surfdata <- paste0("/home/karger/
                       surfdata_0.1x0.1_EUR_hist_16pfts_Irrig_
                       CMIP6_simyr2005_c230523_eunisl3_",nff,".nc")
    
    # copy the surfdata file to a new file
    system(paste0("cp ",surfdata_org," ",surfdata))
    
    # loop over the PFTS
    for (i in c(0:14)) {
        print(paste0(nff,"-",i))
        nffst <- stack(paste0("/storage/karger/feedbacks/",
                              nff,"_pftstack_cormisseunis.tif"))
        pft <- nffst[[i+1]]
        pft[is.na(eunismask)] <- NA
        pft <- as.data.frame(pft)
        df <- as.data.frame(cbind(pft,raster::coordinates(nffst)))
        df[,1] <- df[,1]*100
        modifySurfdata(df, surfdata, i, crs_in, landseamask)
    }
}
```

To check if for all cells the sum of the PFTs sum up to 100, we check
each modified NetCDF file, and if a cell does not sum of to 100, the PFT
fractions will get rescaled using the "scaleSum1" function. The script
is found in the "confirmRange01.R" file:
```R
source("./functions/scaleSum1.R")

for (nff in c("nac","nfn","nfs","ssp1")){
  surfdata <- paste0("/home/karger/
                     surfdata_0.1x0.1_EUR_hist_16pfts_
                     Irrig_CMIP6_simyr2005_c230523_eunisl3_", nff, ".nc")
  
  # read in the PFT data
  r <- raster::brick(surfdata, varname="PCT_NAT_PFT")

  # calculate the sum
  rsum <- calc(r, sum)

  # scale the PFT data to a sum of 100
  rcor <- calc(r, scaleSum1)

  # loop over all PFTs and replace the data with the corrected data.
  for (i in c(1:15)){
      lon <- raster::raster(surfdata, varname="LONGXY")
      lat <- raster::raster(surfdata, varname="LATIXY")
      dfr <- as.data.frame(lon)
      dfr$y <- raster::values(lat)
      dfr$z <- raster::values(rcor[[i]])
      colnames(dfr) <- c("x","y","z")
      rdf <- raster::rasterFromXYZ(dfr)
      # Open the NetCDF file in read-write mode
      ncfile <- nc_open(surfdata, write = TRUE)
      # Get the variable to modify
      var <- ncvar_get(ncfile, "PCT_NAT_PFT")
      rdf_m <- t(as.matrix(rdf))
      rdf_m <- rdf_m[, ncol(rdf_m):1]
      var[,,i] <- rdf_m
      ncvar_put(ncfile, "PCT_NAT_PFT", var)
      nc_close(ncfile)
    }
}
```

## Replace cropland and natural vegetation fractions 

With the PFTs replaced and checked, we also need to adjust the percentages 
cropland vs.natural vegetation in the surface data file. For this we first
derive the respective fractions from the EUNIS data. Here we define cropland 
(mapid=224:236), ice (mapid=218), and urban (mapid=223), with natural vegetation
being the remainder of the EUNIS classes:
```R
library(terra)
ice <- 218
urban <- 223
cropland <- c(224:236)
```

Now we formulate a function that helps us find the EUNIS classes that are 
not in either of the above categories:
```R
'%ni%' <- Negate('%in%')
```

Now we can loop over all NFF scenarios and create a stack of the land 
unit classes and save those as a GEOTiff file seperatly for each NFF: 
```R
for (nff in c("ssp1","nfn","nac","nfs")){
  eunis <- raster(paste0("./data/eunis_map_",nff,".tif"))
  veg <- na.omit(unique(
    values(eunis))[unique(values(eunis)) %ni% c(ice,urban,cropland)])
  r_ice <- eunis
  r_ice[r_ice %ni% ice] <- NA
  r_ice[r_ice %in% ice] <- 100

  r_urban <- eunis
  r_urban[r_urban %ni% urban] <- NA
  r_urban[r_urban %in% urban] <- 100

  r_cropland <- eunis
  r_cropland[r_cropland %ni% cropland] <- NA
  r_cropland[r_cropland %in% cropland] <- 100

  r_veg <- eunis
  r_veg[r_veg %ni% veg] <- NA
  r_veg[r_veg %in% veg] <- 100

  landunits <- c(rast(r_ice),rast(r_urban), rast(r_cropland), rast(r_veg))
  landunits[is.na(landunits)] <- 0

  r_water <- sum(landunits)
  r_water[r_water==0] <- NA
  r_water[r_water==100] <- 0
  r_water[is.na(r_water)] <- 100

  clm_units <- c(landunits, r_water)

  writeRaster(clm_units, paste0("/home/karger/scratch/",
                                nff,"_unitsstack.tif"), overwrite=T)
  }

```

Having calculated the land unit classes, we can now loop over all NFF 
scenarios and replace the respective classes. The script is in the file 
replace_PCTs_NFF.R.

First, source the function for this and define the crs of the surface data:
```R
source("./functions/replacePCT.R")
source("./functions/scaleSum1pct.R")

crs_surfdata <- "+proj=longlat +datum=WGS84 +no_defs +type=crs"
```

Now loop over all NFF scenarios:
```R

for (nff in c("nfn","nfs","ssp1")){
  # load the data
  surfdata <- paste0("/home/karger/surfdata_0.1x0.1_EUR_hist_16pfts_Irrig_
                     CMIP6_simyr2005_c230523_eunisl3_", nff, ".nc")
  lunits <- brick(paste0("/home/karger/scratch/",nff,"_unitsstack.tif"))

  # mask NA values as NA
  lunits[is.na(eunismask)] <- NA
  lunits_crop <- lunits[[3]]
  lunits_natveg <- lunits[[4]]

  # replace cropland fraction
  df <- as.data.frame(lunits_crop)
  df <- as.data.frame(cbind(df, coordinates(lunits)))

  replacePCT(df, surfdata, varid="PCT_CROP", crs_in = crs_in)

  # replace natural vegetation fraction
  df <- as.data.frame(lunits_natveg)
  df <- as.data.frame(cbind(df, coordinates(lunits)))

  replacePCT(df, surfdata, varid="PCT_NATVEG", crs_in = crs_in)

  # check if they sum to 100 
  r_crop <- raster(surfdata, varname="PCT_CROP")
  r_natveg <- raster(surfdata, varname="PCT_NATVEG")
  r_lake <- raster(surfdata, varname="PCT_LAKE")
  r_glacier <- raster(surfdata, varname="PCT_GLACIER")
  r_wetland <- raster(surfdata, varname="PCT_WETLAND")
  r_urban <- calc(brick(surfdata, varname="PCT_URBAN"),sum)
  pcts <- stack(r_crop, r_natveg, r_lake, r_glacier, r_wetland, r_urban)
  values(pcts) <- as.numeric(values(pcts))
  pct_sum <- calc(pcts, sum)

  # correct the pcts to sum up to 100
  pct_cor <- calc(pcts, scaleSum1pct)
  pct_cor_sum <- calc(pct_cor, sum)

  # save the sum for reference
  writeRaster(pct_cor_sum, file="~/pct_cor_sum.tif", format="GTiff", overwrite=T)

  # adjust the crop and natural vegetation after correcting for 100 sum
  ncfile <- nc_open(surfdata, write = TRUE)
    rdf_m <- t(as.matrix(pct_cor[[1]]))
    rdf_m <- rdf_m[, ncol(rdf_m):1]
    ncvar_put(ncfile, "PCT_CROP", rdf_m)
    rdf_m <- t(as.matrix(pct_cor[[2]]))
    rdf_m <- rdf_m[, ncol(rdf_m):1]
    ncvar_put(ncfile, "PCT_NATVEG", rdf_m)
    rdf_m <- t(as.matrix(pct_cor[[3]]))
    rdf_m <- rdf_m[, ncol(rdf_m):1]
  nc_close(ncfile)
}
```

Having replaced all PCTs and PFTs we can now check if the results are sensible
and all PFTs and PCTs are summing up to 100 in the NetCDF file. The 
checkPCT_PFT function writes some maps out to a pdf we define as output:
```R
source('./functions/checkPCT_PFT.R')

for (nff in c("nac","nfn","nfs","ssp1")){
surfdata <- paste0("/home/karger/surfdata_0.1x0.1_EUR_hist_16pfts_Irrig_
                   CMIP6_simyr2005_c230523_eunisl3_",nff,".nc")
output <- paste0('~/pft_pct_sums',nff,'.pdf')
checkPCT_PFT(surfdata = surfdata, output = output, 10, 7, nff)
}
```



## Support
dirk.karger@wsl.ch

## Authors and acknowledgment
Dirk Nikolaus Karger, Johanna T. Malle

## License
CC BY

## Project status
Beta
