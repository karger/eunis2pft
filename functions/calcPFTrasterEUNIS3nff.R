#' Calculates the plant functional type (PFT) fraction from an EUNIS 3 nff map.
#'
#' @param x A vector containing map ID information.
#' @param IVT The InVegetationType (IVT) to consider.
#' @param lookuptable A lookup table containing mapping information.
#'
#' @return The calculated PFT fraction.
#'
#' @examples
#' calcPFTrasterEUNIS3nff(x = c("map_id"), IVT = "IVT_type", lookuptable = lookup_table)
#' 
#' 
#' @export
calcPFTrasterEUNIS3nff <- function(x, IVT, lookuptable){
  out <- 0.0
  try(pfts <- PftfractionFromEUNIS3nff(mapid = x[1],
                                   lookuptable = lookuptable))
  try(out <- pfts[pfts$IVT == IVT, 3])
  #return(as.numeric(out[1]))
  return(out)
}