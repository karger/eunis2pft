
library(raster)
sl_eunis <- read.csv("../data/sl_eunis.csv", sep=";")

sl_eunis_class <- aggregate(cbind(No.of.species, Shannon.index) ~ EUNIS.code, data = sl_eunis, FUN = mean,
                 na.rm = T)

nff <- "nfn"
for (nff in c("nac","nfn","nfs","ssp1")){
eunis <- raster(paste0("./data/eunis_map_",nff,".tif"))

eunis_df <- cbind(as.data.frame(eunis), raster::coordinates(eunis))
eunis_df <- as.data.frame(eunis_df)

colnames(eunis_df) <- c("mapid","x","y")
eunis_legend <- read.csv("../data/eunis_legend.csv", header=T)

eunis_df <- merge(eunis_df, eunis_legend[,2:3], by.x="mapid", by.y="mapid", all=T)
eunis_df <- merge(eunis_df, sl_eunis_class, by.x="EUNIS_2020_code", by.y="EUNIS.code", all=T)

write.table(eunis_df, file=paste0("./data/eunis_df_biodiv_",nff,".txt"))
}


for (nff in c("nac","nfn","nfs","ssp1")){
  eunis_df <- read.table(paste0("./data/eunis_df_biodiv_",nff,".txt"), header=T)
spdf <- terra::vect(eunis_df, geom=c("x", "y"), crs="", keepgeom=FALSE)

eunis_div <- terra::rasterize(spdf,eunis, field="No.of.species")
eunis_shannon <- terra::rasterize(spdf,eunis, field="Shannon.index")
writeRaster(eunis_div,paste0("./data/eunis_div_",nff,".tif"))
writeRaster(eunis_shannon,paste0("./data/eunis_shannon_",nff,".tif"))
}

eunis_legend <- eunis_legend[order(eunis_legend$mapid),]
cols <- c()
for (n in 1:length(eunis_legend$rgbcolor)){
cc <- eval(parse(text=paste0("c",eunis_legend$rgbcolor[n])))
  col <- rgb(red=cc[1],green=cc[2],blue=cc[3], alpha=cc[4], maxColorValue = 255)
  cols <- c(cols, col)
}



pdf("../data/eunis_nff.pdf", height = 40, width =40)
for (nff in c("nac","nfn","nfs","ssp1")){
eunis <- raster(paste0("./data/eunis_map_",nff,".tif"))
plot(eunis, col=cols, main = nff)
}
dev.off()

