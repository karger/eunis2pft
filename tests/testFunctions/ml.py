import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_probability as tfp
tfd = tfp.distributions

import math
import seaborn as sns

# Make NumPy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)


print(tf.__version__)


def plot_loss(history, outfile):
  plt.plot(history.history['loss'], label='loss')
  plt.plot(history.history['val_loss'], label='val_loss')
  plt.ylim([-670, -700])
  plt.xlabel('Epoch')
  plt.ylabel('Error')
  plt.legend()
  plt.grid(True)
  plt.savefig(outfile)




# read in the data
raw_dataset = pd.read_csv('/home/karger/scratch/input_eunis2pft_ML.csv',
                          na_values='?', comment='\t',
                          sep=',', skipinitialspace=True)


# read in the data
raw_dataset = pd.read_csv('/data/df_full.csv',
                          na_values='?', comment='\t',
                          sep=',', skipinitialspace=True)


dataset = raw_dataset.copy()
dataset.tail()
print(dataset.head())
dataset = dataset.dropna()

train_dataset = dataset.sample(frac=0.8, random_state=0)
test_dataset = dataset.drop(train_dataset.index)

train_features = train_dataset.copy()
test_features = test_dataset.copy()

train_features = train_features.drop(columns=["Unnamed: 0","BDS.Boreal", "BDS.Temperate", "BDT.Boreal",
"BDT.Temperate", "BES.Boreal", "BES.Temperate", "BES.Tropical", "BET.Temperate",
"C3.arctic", "C3.Grass", "C4.Grass", "NDT.Boreal", "NES.Boreal",
"NES.Temperate", "NET.Boreal", "NET.Temperate", "NA"])

test_features = test_features.drop(columns=["Unnamed: 0","BDS.Boreal", "BDS.Temperate", "BDT.Boreal",
"BDT.Temperate", "BES.Boreal", "BES.Temperate", "BES.Tropical", "BET.Temperate",
"C3.arctic", "C3.Grass", "C4.Grass", "NDT.Boreal", "NES.Boreal",
"NES.Temperate", "NET.Boreal", "NET.Temperate", "NA"])

train_labels = train_dataset[["BDS.Boreal", "BDS.Temperate", "BDT.Boreal",
"BDT.Temperate", "BES.Boreal", "BES.Temperate", "BES.Tropical", "BET.Temperate",
"C3.arctic", "C3.Grass", "C4.Grass", "NDT.Boreal", "NES.Boreal",
"NES.Temperate", "NET.Boreal", "NET.Temperate"]]

test_labels = test_dataset[["BDS.Boreal", "BDS.Temperate", "BDT.Boreal",
"BDT.Temperate", "BES.Boreal", "BES.Temperate", "BES.Tropical", "BET.Temperate",
"C3.arctic", "C3.Grass", "C4.Grass", "NDT.Boreal", "NES.Boreal",
"NES.Temperate", "NET.Boreal", "NET.Temperate"]]

normalizer = tf.keras.layers.Normalization(axis=-1)
normalizer.adapt(np.array(train_features))

first = np.array(train_features[:1])

with np.printoptions(precision=2, suppress=True):
  print('First example:', first)
  print()
  print('Normalized:', normalizer(first).numpy())




def class_reg_loss(y_true, y_pred):
    squared_difference = tf.square(y_true - y_pred)
    regression_loss = tf.reduce_mean(squared_difference, axis=-1)
    bce = tf.keras.losses.BinaryCrossentropy(from_logits=False)
    classfication_loss = bce((y_true > 1).float(), (y_pred > 1).float())
    return classfication_loss + regression_loss


import numpy as np

y_true = [12, 20, 29., 60.]
y_pred = [14., 18., 27., 55.]
cl = class_reg_loss(np.array(y_true),np.array(y_pred))
cl.numpy()

def build_and_compile_model(norm):
  model = keras.Sequential([norm,
      layers.Dense(200, activation='relu'),
      layers.Dense(200, activation='relu'),
      layers.Dense(16)])
  model.compile(loss=class_reg_loss,
                optimizer=tf.keras.optimizers.Adam(0.001))
  return model


dnn_model = build_and_compile_model(normalizer)

dnn_model.summary()

history = dnn_model.fit(
    train_features,
    train_labels,
    validation_split=0.2,
    verbose=1, epochs=30)

plot_loss(history, '/storage/karger/scratch/history.pdf')

