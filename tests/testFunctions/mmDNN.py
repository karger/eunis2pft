
from tensorflow import feature_column
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
import pandas as pd


#dataframe = read_csv('/home/karger/scratch/input_eunis2pft_ML.csv')
#dataset = dataframe.values
# read in the data
dataframe = pd.read_csv('/data/df_full.csv',
                        na_values='?', comment='\t',
                        sep=' ', skipinitialspace=True)


# Drop un-used columns.
dataframe = dataframe.drop(columns=['EUNIS_l3','habitat', 'EUNIS.2020_description', 'plot_id', 'coords.x1', 'coords.x2'])


# split into training and testing
train, test = train_test_split(dataframe, test_size=0.2)
train, val = train_test_split(train, test_size=0.2)


# A utility method to create a tf.data dataset from a Pandas Dataframe
def df_to_dataset(dataframe, shuffle=True, batch_size=32):
  dataframe = dataframe.copy()
  labels = pd.concat([dataframe.pop(x) for x in ["Bare_ground", "water","BDS.Boreal","BDS.Temperate","BDT.Boreal","BDT.Temperate","BES.Boreal","BES.Temperate","BES.Tropical","BET.Temperate","C3.arctic","C3.Grass","C4.Grass","NDT.Boreal","NES.Boreal","NES.Temperate","NET.Boreal","NET.Temperate"]], axis=1)
  ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
  if shuffle:
    ds = ds.shuffle(buffer_size=len(dataframe))
  ds = ds.batch(batch_size)
  return ds

# A utility method to create a feature column
# and to transform a batch of data
def demo(feature_column):
  feature_layer = layers.DenseFeatures(feature_column)
  print(feature_layer(example_batch).numpy())


batch_size = 32# A small batch sized is used for demonstration purposes
train_ds = df_to_dataset(train, batch_size=batch_size)
val_ds = df_to_dataset(val, shuffle=False, batch_size=batch_size)
test_ds = df_to_dataset(test, shuffle=False, batch_size=batch_size)


for feature_batch, label_batch in train_ds.take(1):
  print('Every feature:', list(feature_batch.keys()))


[27] "CHELSA_bio1_1981.2010_V.2.1"  "CHELSA_bio12_1981.2010_V.2.1"
[29] "CHELSA_bio4_1981.2010_V.2.1"  "CHELSA_bio15_1981.2010_V.2.1"
[31] "CHELSA_gdd5_1981.2010_V.2.1"  "CHELSA_npp_1981.2010_V.2.1"
[33] "CHELSA_swe_1981.2010_V.2.1"


bio1 = feature_column.numeric_column('CHELSA_bio1_1981.2010_V.2.1')

crossed_feature = feature_column.crossed_column([EUNIS_L3, luc], hash_bucket_size=34)
demo(feature_column.indicator_column(crossed_feature))
























# use feature hashing
def hash_function(row):
    return(sklearn.utils.murmurhash3_32(row.EUNIS_l3))

dataframe["eunisl3_hash"] = dataframe.apply(hash_function, axis=1)


n_features = 50

def mod_function(row):
    return(abs(row.eunisl3_hash) % n_features)

dataframe["eunisl3_hash_mod"] = dataframe.apply(mod_function, axis=1)
dataframe.head(5)




dataset = dataframe.values

#dataframe = read_csv('./data/esyPFTs_v1.3.csv')

# split into input (X) and output (y) variables
X, y = dataset[:, 2:269], dataset[:, 271:286]
X, y = X.astype('float'), y.astype('float')
n_features = X.shape[1]

# encode strings to integer
y_cat = y
y_cat[y_cat > 0] = 1
y_class = y_cat
n_class = len(unique(y_class))

# split data into train and test sets
X_train, X_test, y_train, y_test, y_train_class, y_test_class = train_test_split(X, y, y_class, test_size=0.33, random_state=1)

# define the loss function


# create numpy_array
# y_train = np.array([[0, 2, 0, 5, 5], [0, 4, 0, 6, 1]])
# y_test  = np.array([[0.1, 2, 0.6, 0.2, 5], [0.4, 4, 0.6, 3, 0.1]], )

# convert it to tensorflow
# y_true = tf.convert_to_tensor(y_train, tf.float64)
# y_pred = tf.convert_to_tensor(y_test, tf.float64)


#input
visible = Input(shape=(n_features,))
hidden1 = Dense(200, activation='relu', kernel_initializer='he_normal')(visible)
hidden2 = Dense(200, activation='relu', kernel_initializer='he_normal')(hidden1)

# regression output
out_reg = Dense(15, activation=('relu'))(hidden2)
# classification output
# out_clas = Dense(15, activation='softmax')(hidden2)

# define model
#model = Model(inputs=visible, outputs=[out_reg, out_clas])
model = Model(inputs=visible, outputs=[out_reg])

model = Sequential([
    tf.keras.layers.Input(shape = X_train.shape[1:]),
    Dense(300, activation='relu'),
    Dense(300, activation='relu'),
    Dense(300, activation='relu'),
    Dense(300, activation='relu'),
    Dense(300, activation='relu'),
    Dense(300, activation='relu'),
    Dense(15, activation='linear')
])


# plot graph of model
#plot_model(model, to_file='/home/karger/scratch/model.png', show_shapes=True)
model.summary()



def class_reg_loss(y_true, y_pred):
  bce = tf.keras.losses.BinaryCrossentropy(from_logits=False)
  class_loss = bce(tf.math.divide_no_nan(y_true, y_true),
  tf.math.divide_no_nan(y_pred, y_pred))
  mae = tf.keras.losses.MeanAbsoluteError()
  reg_loss = mae(y_pred[y_true > 0], y_true[y_true > 0])
  return class_loss + reg_loss*2



def nozero_reg_loss(y_true, y_pred):
  mae = tf.keras.losses.MeanAbsoluteError()
  reg_loss = mae(y_pred[y_true > 0], y_true[y_true > 0])
  return reg_loss


# compile the keras model
model.compile(loss=mae,
              optimizer=tf.keras.optimizers.Adam(0.0001))

# fit the keras model on the dataset
hist = model.fit(X_train, y_train, epochs=15, batch_size=32, verbose=1)


def plot_loss(history):
  plt.plot(history.history['loss'], label='loss')
  plt.plot(history.history['loss'], label='loss')
  plt.ylim([0.0, 0.0085])
  plt.xlabel('Epoch')
  plt.ylabel('Error')
  plt.legend()
  plt.grid(True)
  plt.savefig('/storage/karger/scratch/hist.pdf')


plot_loss(hist)

# make predictions on test set
y_predictions = model.predict(X_test)

def plot_obs_mod(x, y, n):
    plt.plot(x[[n]], y[[n]], color='k', label='Predictions')
    plt.xlabel('predictions')
    plt.ylabel('observations')
    plt.savefig('/storage/karger/scratch/obs_mod.pdf')


plot_obs_mod(y_predictions, y_test, 1)

