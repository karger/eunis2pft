# Load the ncdf4 package
library(ncdf4)
library(raster)

lookuptable <- read.csv("D:/bio/Bio_Backup/Running_Projects/Feedbacks/eunis_pfts/esyPFTs_v1.0.txt", sep="\t")
eunis <- terra::vect("D:/bio/Bio_Backup/Running_Projects/Feedbacks/eunis_pfts/lvl2.shp")

ext1 <- terra::ext(4017432,4424605,2513213,2742081)
eunis_s <- terra::crop(eunis, ext1)

pft = "BDT Temperate"
ITV <- 7
level = 2
df <- SpEunis2Pft(eunis_s, lookuptable, level, pft, use.ivt=FALSE)

write.table(df, file="C:/scratch/test_res.txt", sep="\t", row.names=F)

surfdata <- "Z:/karger/surfdata_0.1x0.1_hist_16pfts_Irrig_CMIP6_simyr2000_c200915_Copy.nc"

crs_in <- crs(eunis)
df[,1] <- df[,1]*100
modifySurfdata(df, surfdata, ITV, crs_in)


### create input data
level <- 2
lookuptable <- read.csv("~/scripts/eunis_pfts/data/esyPFTs_v1.0.txt", sep="\t")
eunis <- terra::vect("~/scripts/eunis_pfts/data/lvl2.shp")
surfdata <- "/mnt/lud11/karger/surfdata_0.1x0.1_hist_16pfts_Irrig_CMIP6_simyr2000_c200915_eunis.nc"

eunisdf <- data.frame(matrix(ncol=2,nrow=19))
colnames(eunisdf) <- c("ITV", "PFT")
eunisdf$PFT <- c("Bare Ground", "NET Temperate", "NET Boreal", "NDT Boreal", "BET Tropical", 
                 "BET Temperate", "BDT Tropical", "BDT Temperate", "BDT Boreal", "BES Temperate",
                 "BDS Temperate", "BDS Boreal", "C3 arctic", "C3 Grass", "C4 Grass", "UCrop UIrr", 
                 "UCrop Irr", "Crop UIrr", "Crop Irr")
eunisdf$ITV <- seq(0,18,1)

# loop over all PFTs
for (i in unique(eunisdf$ITV))
{
    print(i)
    pft <- eunisdf$PFT[i+1]
    df <- SpEunis2Pft(eunis, lookuptable, level, pft, use.ivt=FALSE, parallel=TRUE)
    fileout <- paste0("/storage/karger/EUNIS/eunispftdf/",gsub(" ","_",pft),"_df.txt")
    write.table(df, file=fileout, sep="\t", row.names=F)
    modifySurfdata(df, surfdata, ITV, crs_in)
}


for(eunisclass in unique(vec))
{
    print(eunisclass)
    df1 <- try(PftfractionFromEUNIS(eunisclass, lookuptable, level=3))
    print(df1)
}
